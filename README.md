# Docker Authentication Provider Service #

### What is this repository for? ###

* Contains source code for a service that stores docker repository credentials
* Version: 1.2.1

### How do I get set up? ###

* To build run "./gradlew build"
* To start - copy fat jar from bin/libs to DC/OS master machine and execute
```
#! /bin/sh
DIR=`dirname $0`
cd $DIR
echo Working directory: `pwd`
java -jar docker_auth.jar \
          -clusterhost=`~/getip` \
          -usedefaultclusterhost=false \
          -serviceStdDebugOutput=true \
          -zip_config=true \
          -mongouri=mongodb://ddementiev0d1.ptcnet.ptc.com:27017
```

### Who do I talk to? ###

* Author: Daniel Dementiev
* Contact: ddementiev@ptc.com
