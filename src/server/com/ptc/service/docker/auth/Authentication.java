package com.ptc.service.docker.auth;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.sm.javax.langx.Strings;

public class Authentication
{
	private String artifactory;
	private String user;
	private byte[] authentication;
	
	public Authentication (String artifactory, String user, byte[] authentication)
	{
		super ();
		this.artifactory = artifactory;
		this.user = user;
		this.authentication = authentication;
	}

	public String getArtifactory ()
	{
		return artifactory;
	}

	public String getUser ()
	{
		return user;
	}

	public byte[] getAuthentication ()
	{
		return authentication;
	}

	public void setAuthentication (byte[] auth)
	{
		this.authentication = auth;
	}
	
	@Override
	public boolean equals (Object obj)
	{
		if (obj != null && obj instanceof Authentication) {
			Authentication auth = (Authentication) obj;
			if ((artifactory.equals (auth.artifactory) && user.equals (auth.user))) {
				return sameCredentials (auth.authentication);
			}
			return false;
		}
		return super.equals (obj);
	}

	@Override
	public int hashCode ()
	{
		return (buildKey (artifactory, user).hashCode ()^2 | authentication.hashCode ());
	}
	
	public static String buildKey (String artifactory, String user)
	{
		return Strings.concat (user, "@", artifactory);
	}
	
	@Override
	public String toString ()
	{
		return buildKey (artifactory, user) + "  (" + byteArrayToString (authentication) + ")";
	}

	public static String byteArrayToString (byte[] data)
	{
		StringBuilder sb = new StringBuilder ();
		if (data != null) {
			sb.append ("{");
			for (byte b : data) {
				sb.append (String.format ("%02x ", b));
			}
			sb.append ("}");
		}
		else {
			sb.append ("null");
		}
		return sb.toString ();
	}

	public boolean sameCredentials (byte[] credentials)
	{
		if (credentials == null || authentication.length != credentials.length) {
			return false;
		}
		for (int i=0; i<authentication.length; i++) {
			if (authentication [i] != credentials [i]) {
				return false;
			}
		}
		return true;
	}

	public void writeTo (DataOutputStream out) throws IOException
	{
		out.writeUTF (artifactory);
		out.writeUTF (user);
		out.writeInt (authentication.length);
		out.write (authentication);
	}
	
	public static Authentication readFrom (DataInputStream inp) throws IOException
	{
		String artifactory = inp.readUTF ();
		String user = inp.readUTF ();
		int len = inp.readInt ();
		byte[] auth = new byte [len];
		if (inp.read (auth) != len) {
			throw new IOException ("Failed to read authentication info");
		}
		return new Authentication (artifactory, user, auth);
	}

}
