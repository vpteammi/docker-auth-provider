package com.ptc.service.docker.auth.test;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import com.ptc.service.docker.auth.Authentication;
import com.ptc.service.docker.auth.DockerAuthenticationStorage;

public class CreateAuthTestRunner
{

	public static void main (String[] args)
	{
		try {
			String artifactory = "ccp-docker.artifacts.devops.ptc.io";
			DockerAuthenticationStorage provider = new DockerAuthenticationStorage ();
			provider.addAuthentication (artifactory, "ccp-publisher", "rJDn2U");
			Authentication auth = provider.getAuthentication (artifactory);
			output (auth.getAuthentication (), artifactory + ".tar.gz");
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void output (byte[] authentication, String fname)
	{
		try (DataOutputStream out = new DataOutputStream (new FileOutputStream (fname))) {
			out.write (authentication);
			System.out.println ("Authentication is written to " + fname);
		}
		catch (IOException e) {
			e.printStackTrace ();
		}
	}

}
