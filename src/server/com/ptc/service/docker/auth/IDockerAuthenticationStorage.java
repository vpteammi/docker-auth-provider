package com.ptc.service.docker.auth;

import java.io.IOException;
import java.util.function.Consumer;

import com.sm.javax.query.Result;

public interface IDockerAuthenticationStorage extends IAuthenticationStorage
{
	public default void addAuthentication (String artifactory, String user, String password) throws IOException
	{
		addAuthentication (artifactory, user, new DockerAuthentication (artifactory, user, password));
	}
	
	public default void addAuthentication (String artifactory, String user, String password, 
			Consumer<Result<Boolean>> handler)
	{
		System.out.println ("DockerAuthenticationStorage::addAuthentication >>>");
		DockerAuthentication.asyncCreator (artifactory, user, password, res -> {
			if (res.succeeded ()) {
				addAuthentication (artifactory, user, res.value (), handler);
			}
			else {
				handler.accept (new Result<Boolean> (res.cause ()));
			}
		});
		System.out.println ("DockerAuthenticationStorage::addAuthentication <<<");
	}
	
}
