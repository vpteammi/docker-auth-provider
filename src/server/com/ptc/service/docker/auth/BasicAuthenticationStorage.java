package com.ptc.service.docker.auth;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.Consumer;

import com.sm.javax.langx.Strings;
import com.sm.javax.query.Result;

public class BasicAuthenticationStorage implements IAuthenticationStorage
{
	private HashMap<String, List<Authentication>> artifactoryAuthMap; 
	private HashMap<String, Authentication>       userAuthMap;
	private boolean                               isLoadingData;
	private String                                localStorageFileName;
	private IAuthenticationStorage                backupStorage;
	
	public BasicAuthenticationStorage (String fileName)
	{
		super ();
		artifactoryAuthMap = new HashMap<> ();
		userAuthMap = new HashMap<> ();
		localStorageFileName = fileName;
		isLoadingData = false;
		if (! Strings.isEmpty (localStorageFileName)) {
			try {
				loadFrom (localStorageFileName);
			}
			catch (IOException e) {
				System.out.println ("Failed to load authentication info from " + localStorageFileName + ": " + e.getMessage ());
			}
		}
	}
	
	@Override
	public void setBackupStorage (IAuthenticationStorage backupStorage)
	{
		this.backupStorage = backupStorage;
	}

	@Override
	public String getLocalStorage ()
	{
		return localStorageFileName;
	}
	
	@Override
	public Authentication getAuthentication (String artifactory)
	{
		System.out.println ("Getting auth info from artifactoryAuthMap for " + artifactory);
		List<Authentication> auths = artifactoryAuthMap.get (artifactory);
		if (auths != null && ! auths.isEmpty ()) {
			return auths.get (0);
		}
		System.out.println ("No Info found");
		return null;
	}

	@Override
	public void getAuthentication (String artifactory, Consumer<Result<Authentication>> handler)
	{
		Authentication auth = getAuthentication (artifactory);
		if (auth != null) {
			handler.accept (new Result<> (auth));
		}
		else {
			if (backupStorage != null) {
				backupStorage.getAuthentication (artifactory, res -> {
					if (res.succeeded ()) {
						add (res.value ());
					}
					handler.accept (res);
				});
			}
			else {
				handler.accept (new Result<Authentication> ("Failed to retrieve authentication for " + artifactory));
			}
		}
	}

	@Override
	public Authentication getAuthentication (String artifactory, String userName)
	{
		if (Strings.isEmpty (userName)) {
			return getAuthentication (artifactory);
		}
		System.out.println ("Getting auth info from userAuthMap for " + Authentication.buildKey (artifactory, userName));
		return userAuthMap.get (Authentication.buildKey (artifactory, userName));
	}

	@Override
	public void getAuthentication (String artifactory, String userName, Consumer<Result<Authentication>> handler)
	{
		Authentication auth = getAuthentication (artifactory, userName);
		if (auth != null) {
			handler.accept (new Result<> (auth));
		}
		else {
			if (backupStorage != null) {
				backupStorage.getAuthentication (artifactory, userName, res -> {
					if (res.succeeded ()) {
						add (res.value ());
					}
					handler.accept (res);
				});
			}
			else {
				handler.accept (new Result<Authentication> ("Failed to retrieve authentication for " + 
						Authentication.buildKey (artifactory, userName)));
			}
		}
	}

	// -----------------------------------------------------------------------------------------------
	private boolean add (String artifactory, String userName, Authentication authentication)
	{
		String key = Authentication.buildKey (artifactory, userName);
		Authentication existingAuth = userAuthMap.get (key);
		boolean isUpdated = true;
		if (existingAuth != null) {
			if (! existingAuth.equals (authentication)) {
				existingAuth.setAuthentication (authentication.getAuthentication ());
			}
			else {
				isUpdated = false;
			}
		}
		else {
			userAuthMap.put (key, authentication);
			List<Authentication> auths = artifactoryAuthMap.get (artifactory);
			if (auths == null) {
				auths = new LinkedList<> ();
				artifactoryAuthMap.put (artifactory, auths);
			}
			auths.add (authentication);
		}
		//
		if (isUpdated) {
			save ();
			printAuthenticationInfo ();
		}
		return isUpdated;
	}
	
	private boolean add (Authentication auth)
	{
		return add (auth.getArtifactory (), auth.getUser (), auth);
	}
	
	@Override
	public void addAuthentication (String artifactory, String userName, Authentication authentication)
	{
		boolean isUpdated = add (artifactory, userName, authentication);
		if (isUpdated && backupStorage != null) {
			backupStorage.addAuthentication (artifactory, userName, authentication, res -> {
				System.out.println ("Sent ADD request to backup storage");
			});
		}
	}
	
	@Override
	public void addAuthentication (String artifactory, String userName, Authentication authentication, 
			Consumer<Result<Boolean>> handler)
	{
		addAuthentication (artifactory, userName, authentication);
		handler.accept (new Result<> (true));
	}
	
	// -----------------------------------------------------------------------------------------------
	@Override
	public void deleteAuthentication (String artifactory, String userName)
	{
		if (backupStorage != null) {
			backupStorage.deleteAuthentication (artifactory, userName, res -> {
				System.out.println ("Sent DELETE request to backup storage");
			});
		}
		if (Strings.isEmpty (userName)) {
			artifactoryAuthMap.remove (artifactory);
		}
		else {
			Authentication auth = userAuthMap.remove (Authentication.buildKey (artifactory, userName));
			List<Authentication> auths = artifactoryAuthMap.get (artifactory);
			if (auths != null) {
				auths.remove (auth);
				if (auths.size () == 0) {
					artifactoryAuthMap.remove (artifactory);
				}
				save ();
			}
		}
	}
	
	@Override
	public void deleteAuthentication (String artifactory, String userName, 
			Consumer<Result<Boolean>> handler)
	{
		deleteAuthentication (artifactory, userName);
		handler.accept (new Result<> (true));
	}
	
	// -----------------------------------------------------------------------------------------------
	protected void save ()
	{
		String fileName = getLocalStorage ();
		if (! isLoadingData && ! Strings.isEmpty (fileName)) {
			try {
				saveTo (fileName);
			}
			catch (IOException e) {
				System.out.println ("Error - failed to save authentication info to " + fileName + ": " + e.getMessage ());
			}
		}
	}
	
	@Override
	public void saveTo (String fileName) throws IOException
	{
		DataOutputStream out = new DataOutputStream (new FileOutputStream (fileName));
		Collection<Authentication> auths = userAuthMap.values ();
		out.writeInt (auths.size ());
		for (Authentication auth : auths) {
			auth.writeTo (out);
		}
		out.close ();
	}
	
	@Override
	public void loadFrom (String fileName) throws IOException
	{
		isLoadingData = true;
		try {
			DataInputStream inp = new DataInputStream (new FileInputStream (fileName));
			int len = inp.readInt ();
			for (int i=0; i<len; i++) {
				add (Authentication.readFrom (inp));
			}
			inp.close ();
		}
		finally {
			isLoadingData = false;
		}
	}

	// -----------------------------------------------------------------------------------------------
	public void addAuthentication (Authentication authentication)
	{
		addAuthentication (authentication.getArtifactory (), authentication.getUser (), authentication);
	}

	private void printAuthenticationInfo ()
	{
		System.out.println ("User@artifactory map: {");
		for (Entry<String, Authentication> entry : userAuthMap.entrySet ()) {
			System.out.println ("  " + entry.getKey () + " -> " + entry.getValue ());
		}
		System.out.println ("}");
		System.out.println ("Artifactory map: {");
		for (Entry<String, List<Authentication>> entry : artifactoryAuthMap.entrySet ()) {
			System.out.println ("  " + entry.getKey () + " -> {");
			List<Authentication> auths = entry.getValue ();
			if (auths != null) {
				for (Authentication auth : auths) {
					System.out.println ("    " + auth);
				}
			}
			System.out.println ("  }");
		}
		System.out.println ("}");
	}

}
