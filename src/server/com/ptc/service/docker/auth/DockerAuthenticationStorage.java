package com.ptc.service.docker.auth;

public class DockerAuthenticationStorage extends BasicAuthenticationStorage implements IDockerAuthenticationStorage
{
	public DockerAuthenticationStorage (String fileName)
	{
		super (fileName);
	}
	
	public DockerAuthenticationStorage ()
	{
		super (null);
	}
	
}
