package com.ptc.service.docker.auth;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;

import com.ptc.dbservices.EmbededDocument;
import com.ptc.dbservices.IDocument;
import com.ptc.dbservices.IEmbededDocument;
import com.ptc.dbservices.IEmbededDocument.PathNotFoundException;
import com.ptc.dbservices.mongo.MongoCollection;
import com.sm.javax.langx.Strings;
import com.sm.javax.query.Result;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;

public class MongoAuthenticationStorage implements IDockerAuthenticationStorage
{
	
	private MongoClient mongoClient;
	private MongoCollection mongoCollection;
	
	public MongoAuthenticationStorage (Vertx vertx, String mongoUri, String mongoDB, String collectionName)
	{
		super ();
		JsonObject mongoConfig = new JsonObject ().put ("connection_string", mongoUri).put ("db_name", mongoDB);
		mongoClient = MongoClient.createShared (vertx, mongoConfig);
		mongoCollection = new MongoCollection (mongoClient, collectionName);
	}
	
	
	private static final String ArtifactoryField = "artifactory";
	private static final String UserField = "user";
	private static final String AuthenticationField = "authentication";
	/*
	 * Mongo document structure:
	 * {
	 * 		artifactory:    <string>      - URL of the docker repository
	 * 		user:           <string>      - user name of docker repo
	 *      authentication: <BLOB byte[]> - binary data containing docker.tar.gz
	 * }
	 */

	// -------------------------------------------------------------------------------
	@Override
	public Authentication getAuthentication (String artifactory)
	{
		return getAuthentication (artifactory, (String) null);
	}

	@Override
	public Authentication getAuthentication (String artifactory, String userName)
	{
		CompletableFuture<Authentication> future = new CompletableFuture<Authentication> ();
		getAuthentication (artifactory, userName, res -> {
			if (res.succeeded ()) {
				future.complete (res.value ());
			}
			else {
				future.completeExceptionally (res.cause ());
			}
		});
		try {
			return future.get ();
		}
		catch (InterruptedException | ExecutionException e) {
			return null;
		}
	}

	// -------------------------------------------------------------------------------
	@Override
	public void getAuthentication (String artifactory, Consumer<Result<Authentication>> handler)
	{
		getAuthentication (artifactory, null, handler);
	}

	@Override
	public void getAuthentication (String artifactory, String userName, Consumer<Result<Authentication>> handler)
	{
		JsonObject query = new JsonObject ();
		query.put (ArtifactoryField, artifactory);
		if (! Strings.isEmpty (userName)) {
			query.put (UserField, userName);
		}
		mongoCollection.findDocument (query, res -> {
			if (res.succeeded ()) {
				IDocument doc = res.value ();
				try {
					handler.accept (new Result<Authentication> (documentToAuthentication (doc)));
				}
				catch (PathNotFoundException e) {
					handler.accept (new Result<> (e));
				}
			}
			else {
				handler.accept (new Result<> (res.cause ()));
			}
		});
	}

	// -------------------------------------------------------------------------------
	@Override
	public void addAuthentication (String artifactory, String userName, Authentication auth)
	{
		CompletableFuture<Boolean> future = new CompletableFuture<Boolean> ();
		addAuthentication (artifactory, userName, auth, res -> {
			if (res.succeeded ()) {
				future.complete (res.value ());
			}
			else {
				future.completeExceptionally (res.cause ());
			}
		});
		try {
			future.get ();
		}
		catch (InterruptedException | ExecutionException e) {
		}
	}

	@Override
	public void addAuthentication (String artifactory, String userName, Authentication auth,
			Consumer<Result<Boolean>> handler)
	{
		try {
			IEmbededDocument doc = new EmbededDocument ()  // mongoCollection.createDocument ()
				.setProperty (ArtifactoryField, artifactory)
				.setProperty (UserField, userName);
			mongoCollection.findDocument (doc, (JsonObject) null, res -> {
				boolean insertNew = true;
				if (res.succeeded ()) {
					IDocument existingDoc = res.value ();
					try {
						insertNew = false;
						if (auth.sameCredentials (existingDoc.getPropertyBLOBValue (AuthenticationField))) {
							handler.accept (new Result<Boolean> (true));
						}
						else {
							updateAuthentication (existingDoc, auth, handler);
						}
					}
					catch (PathNotFoundException e) {
						insertNew = true;
					}
				}
				if (insertNew) {
					addAuthentication (doc.setProperty (AuthenticationField, auth.getAuthentication ()), handler);
				}
			});
			doc.setProperty (AuthenticationField, auth.getAuthentication ());
		}
		catch (Exception e) {
			handler.accept (new Result<> (e));
		}
	}

	private void addAuthentication (IEmbededDocument doc, Consumer<Result<Boolean>> handler)
	{
		mongoCollection.insertDocument (doc, res -> {
			if (res.succeeded ()) {
				handler.accept (new Result<Boolean> (true));
			}
			else {
				handler.accept (new Result<> (res.cause ()));
			}
		});
	}

	private void updateAuthentication (IDocument existingDoc, Authentication auth, Consumer<Result<Boolean>> handler)
	{
		existingDoc.update ().property (AuthenticationField).set (auth.getAuthentication ()).execute (res -> {
			if (res.succeeded ()) {
				handler.accept (new Result<Boolean> (true));
			}
			else {
				handler.accept (new Result<> (res.cause ()));
			}
		});
		/*
		IEmbededDocument doc = new EmbededDocument ().setProperty (AuthenticationField, auth.getAuthentication ());
		JsonObject update = new JsonObject ();
		update.put ("$set", doc.toJson ());
		mongoCollection.updateDocuments (existingDoc.getIdJson (), update, res -> {
			if (res.succeeded ()) {
				handler.accept (new Result<Boolean> (true));
			}
			else {
				handler.accept (new Result<> (res.cause ()));
			}
		});
		*/
	}

	// -------------------------------------------------------------------------------
	@Override
	public void deleteAuthentication (String artifactory, String userName, Consumer<Result<Boolean>> handler)
	{
		JsonObject query = new JsonObject ();
		query.put (ArtifactoryField, artifactory);
		if (! Strings.isEmpty (userName)) {
			query.put (UserField, userName);
		}
		mongoCollection.removeDocuments (query, res -> {
			if (res.succeeded ()) {
				handler.accept (new Result<Boolean> (true));
			}
			else {
				handler.accept (new Result<> (res.cause ()));
			}
		});
	}

	@Override
	public void deleteAuthentication (String artifactory, String userName)
	{
		CompletableFuture<Boolean> future = new CompletableFuture<Boolean> ();
		deleteAuthentication (artifactory, userName, res -> {
			if (res.succeeded ()) {
				future.complete (res.value ());
			}
			else {
				future.completeExceptionally (res.cause ());
			}
		});
		try {
			future.get ();
		}
		catch (InterruptedException | ExecutionException e) {
		}
	}
	
	// -------------------------------------------------------------------------------
	@Override
	public void saveTo (String fileName) throws IOException
	{
		/* do nothing */
	}
	
	@Override
	public void loadFrom (String fileName) throws IOException
	{
		/* do nothing */
	}

	// -------------------------------------------------------------------------------
	private static Authentication documentToAuthentication (IDocument doc) throws PathNotFoundException
	{
		String artifactory = doc.getPropertyStringValue (ArtifactoryField);
		String user = doc.getPropertyStringValue (UserField);
		byte[] auth = doc.getPropertyBLOBValue (AuthenticationField);
		return new Authentication (artifactory, user, auth);
	}

	@Override
	public void setBackupStorage (IAuthenticationStorage backupStorage)
	{
		// do nothing
	}

}
