package com.ptc.service.docker.auth;

import java.io.IOException;
import java.util.function.Consumer;

import com.sm.javax.query.Result;

public interface IAuthenticationStorage
{
	Authentication getAuthentication (String artifactory);
	Authentication getAuthentication (String artifactory, String userName);
	
	void addAuthentication (String artifactory, String userName, Authentication auth);
	
	void deleteAuthentication (String artifactory, String userName);
	default void deleteAuthentication (String artifactory)
	{
		deleteAuthentication (artifactory, (String) null);
	}

	// -------------------------------------------------------------------------------
	void getAuthentication (String artifactory, Consumer<Result<Authentication>> handler);
	void getAuthentication (String artifactory, String userName, Consumer<Result<Authentication>> handler);
	
	void addAuthentication (String artifactory, String userName, Authentication auth, Consumer<Result<Boolean>> handler);
	
	void deleteAuthentication (String artifactory, String userName, Consumer<Result<Boolean>> handler);
	default void deleteAuthentication (String artifactory, Consumer<Result<Boolean>> handler)
	{
		deleteAuthentication (artifactory, null, handler);
	}

	// -------------------------------------------------------------------------------
	public default String getLocalStorage () 
	{
		return null;
	}

	void saveTo (String fileName) throws IOException;
	void loadFrom (String fileName) throws IOException;
	
	void setBackupStorage (IAuthenticationStorage backupStorage);
}
