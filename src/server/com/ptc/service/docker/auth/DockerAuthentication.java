package com.ptc.service.docker.auth;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Consumer;

import io.reactivex.schedulers.Schedulers;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import com.sm.javax.langx.Strings;
import com.sm.javax.langx.annotation.SettingProperty;
import com.sm.javax.query.Result;
import com.sm.javax.utilx.ApplicationProperties;

public class DockerAuthentication extends Authentication
{
	private static Logger logger = LoggerFactory.getLogger (DockerAuthentication.class);
	
	@SettingProperty (defaultValue="false", cmdLineName="docker_auth_debug_mode",
			documentation="Create Docker authentication in debug mode; that is, don't use docker 'login command' and create some string instead.")
	private static boolean DebugMode;
	
	@SettingProperty (defaultValue="false", cmdLineName="zip_config",
			documentation="Store .docker/config.json in tar.zip format (by default store it as is.)")
	private static boolean ZipConfigFile;
	
	static {
		ApplicationProperties.processStaticProperties (DockerAuthentication.class);
	}

	public DockerAuthentication (String artifactory, String user, String password) throws IOException
	{
		super (artifactory, user, buildDcokerAuthentication (artifactory, user, password));
	}
	
	public static void asyncCreator (String artifactory, String user, String password, Consumer<Result<Authentication>> handler)
	{
		System.out.println ("DockerAuthentication::asyncCreator >>>");
		Schedulers.computation ().scheduleDirect (() -> {
			try {
				DockerAuthentication auth = new DockerAuthentication (artifactory, user, password);
				handler.accept (new Result<> (auth));
			}
			catch (IOException ex) {
				handler.accept (new Result<> (ex));
			}
		});
		System.out.println ("DockerAuthentication::asyncCreator <<<");
	}
	
	private static File getHomeDirectory ()
	{
		String homeDir = System.getenv ("HOME");
		if (Strings.isEmpty (homeDir)) {
			System.out.println ("Environment variable HOME is not set");
			return null;
		}
		File dir = new File (homeDir);
		if (dir.exists () && dir.isDirectory ()) {
			return dir;
		}
		return null;
	}
	
	private static int executeCommand (File runDir, String command) throws IOException, InterruptedException
	{
		logInfo ("Executing: " + command);
		ProcessBuilder pbuilder = new ProcessBuilder (command.split (" "))
				.directory (runDir);
		Process proc = pbuilder.start ();
		int exitCode = proc.waitFor ();
		System.out.println ("STDOUT:\n" + readStream (proc.getInputStream ()));
		System.out.println ("STDERR:\n" + readStream (proc.getErrorStream ()));
		System.out.println ("--------------");
		return exitCode;
	}
	
	private static String readStream (InputStream inp)
	{
		StringBuilder sb = new StringBuilder ();
		try (BufferedReader reader = new BufferedReader (new InputStreamReader (inp))) {
			String line;
			while ((line = reader.readLine ()) != null) {
				sb.append (line).append ("\n");
			}
		}
		catch (IOException e) { }
		return sb.toString ();
	}
	
	private static void printBytes (byte[] data)
	{
		int i = 0;
		for (byte b : data) {
			i ++;
			System.out.print (String.format ("%02x ", b));
			if (i % 16 == 0) {
				System.out.print (" ");
			}
			if (i % 32 == 0) {
				System.out.println ();
			}
		}
		System.out.println ();
	}

	private static byte[] buildDcokerAuthentication (String artifactory, String user, String password) throws IOException
	{
		if (DebugMode) {
			return Authentication.buildKey (artifactory, user + "/" + password).getBytes ();
		}
		// execute the following external commands:
		// ---
		// docker login -u <user> -p <password> <artifactory>
		// tar -czvf temp/$1.tar.gz .docker/config.json 
		// docker logout $1
		// ---
		File runDir = getHomeDirectory ();
		if (runDir != null) {
			System.out.println ("Home directory: " + runDir.getAbsolutePath ());
			Path resultFile = Files.createTempFile (artifactory, ".auth");
			String zipCommand = String.format ("tar -czvf %s .docker/config.json", resultFile.toFile ().getAbsolutePath ());
			String copyCommand = String.format ("cp .docker/config.json %s", resultFile.toFile ().getAbsolutePath ()); 
			String[] commands = {
					String.format ("docker login -u %s -p %s %s", user, password, artifactory),
					ZipConfigFile ? zipCommand : copyCommand,
					String.format ("docker logout %s", artifactory)
			};
			for (int i=0; i<commands.length; i++) {
				String command = commands [i];
				try {
					executeCommand (runDir, command);
				}
				catch (InterruptedException e) {
					if (i < commands.length - 1) {
						logError ("Failed to execute command '", e);
						throw new IOException ("Failed to execute command '" + command + "': " + e.getMessage ());
					}
				}
			}
			//
			byte[] auth = Files.readAllBytes (resultFile);
			printBytes (auth);
			return auth;
		}
		else {
			logError ("Unable to find home directory");
			throw new IOException ("Unable to find home directory");
		}
	}
	
	private static void logInfo (String msg)
	{
		logger.info (msg);
		System.out.println (msg);
	}

	private static void logError (String msg, Throwable err)
	{
		logger.error (msg, err);
		System.out.println (msg + ": " + err.getMessage ());
	}

	private static void logError (String msg)
	{
		logger.error (msg);
		System.out.println (msg);
	}
	
	public static boolean getZipConfigFile ()
	{
		return ZipConfigFile;
	}

}
