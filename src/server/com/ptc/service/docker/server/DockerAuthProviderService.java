package com.ptc.service.docker.server;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.BiConsumer;

import com.ptc.ccp.vertx.utils.JsonTools;
import com.ptc.ccp.vertxservices.AbstractRestService;
import com.ptc.service.docker.auth.Authentication;
import com.ptc.service.docker.auth.DockerAuthentication;
import com.ptc.service.docker.auth.DockerAuthenticationStorage;
import com.ptc.service.docker.auth.IDockerAuthenticationStorage;
import com.ptc.service.docker.auth.MongoAuthenticationStorage;
import com.sm.javax.langx.Strings;
import com.sm.javax.langx.annotation.SettingProperty;
import com.sm.javax.utilx.ApplicationProperties;
import com.sm.javax.utilx.ApplicationProperties.CommandLineArgument;
import com.sm.javax.utilx.CommandLineArguments;

import io.vertx.core.Handler;
import io.vertx.core.MultiMap;
import io.vertx.core.VertxOptions;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class DockerAuthProviderService extends AbstractRestService
{

	private static Logger logger = LoggerFactory.getLogger (DockerAuthProviderService.class);	
	
	private static final String DefaultPropertiesFile = "docker_auth.properties";

	// ----------------------------------------------------------------------------------
	@SettingProperty (defaultValue="8880", cmdLineName="port", envVariableName="WEB_DISCOVERY_SERVER_PORT",
			documentation="HTTP server port")
	private static int WebPort;
	
	@SettingProperty (defaultValue="/dcauth", documentation="Web service suffix. To access this service"
			+ " through http use http://<hostname>:${WebPort}/${WebServerPath}")
	private static String WebServerPath;

	@SettingProperty (defaultValue="/debug/*", documentation="Debug path for web service debug access (/debug/*)")
	private static String DebugPath;
	
	@SettingProperty (defaultValue="debug.html", documentation="Debug index page (debug.html)")
	private static String DebugIndexPage;
	
	@SettingProperty (defaultValue="www", documentation="Location of static web pages (www). For example, "
			+ "name of the debug index page will be ${WebRoot}/${DebugIndexPage}")
	private static String WebRoot;
	
	@SettingProperty (defaultValue="", //"mongodb://ddementiev0d1.ptcnet.ptc.com:27017",
			envVariableName="MONGO_URI",
			cmdLineName="mongouri",
			documentation="Full address of mongo DB in format mongodb://<host>:<port> (e.g. 'mongodb://ddementiev0d1.ptcnet.ptc.com:27017')")
	private static String MongoURI;
	
	@SettingProperty (defaultValue="VPCloud", envVariableName="mongodbname", cmdLineName="dbname", 
			documentation="Database name")
	private static String DBName = "VPCloud";

	@SettingProperty (defaultValue="DockerAuthentication", cmdLineName="collection-name", envVariableName="mongo_collection_name",
			documentation="Name of the Mongo collection to store authentication info in.")
	private static String DBCollection;
	
	@SettingProperty (defaultValue="docker_auth_info.dat", cmdLineName="localfile", 
			documentation="Name of the file for storing authentication info locally")
	private static String LocalStorageFileName;
	
	// ----------------------------------------------------------------------------------
	private HashMap<String, RequestAction> commands;
	private IDockerAuthenticationStorage   authStorage;
	
	public DockerAuthProviderService ()
	{
		super (AccessMethod.Web, WebPort);
		initCommands ();
		authStorage = new DockerAuthenticationStorage (LocalStorageFileName);
	}

	// ----------------------------------------------------------------------------------
	private void initCommands ()
	{
		commands = new HashMap<> ();
		addCommand ("get", this::executeGetAuthRequest, "Retrieve authetication file for the URL", 
				new CommandArgument ("artifactory", "URL of the service"), 
				new CommandArgument ("user", "Login name of the user (default=null [that is, any])", true));
		addCommand ("add", this::executeAddAuthRequest, "Add authentication informtaion for the given URL",
				new CommandArgument ("artifactory", "URL of the service"), 
				new CommandArgument ("user", "Login name of the user"),
				new CommandArgument ("password", "Password of the provided user"));
		addCommand ("delete", this::executeDeleteAuthRequest, "Remove authentication information from the storage",
				new CommandArgument ("artifactory", "URL of the service"), 
				new CommandArgument ("user", "Login name of the user (default=null [that is, any])", true));
		addCommand ("help", this::executeHelpRequest, null);
	}
	
	private void addCommand (String command, BiConsumer<JsonObject, Handler<JsonObject>> cmdProcessor, String documentation, CommandArgument ... args)
	{
		commands.put (command, new RequestAction (command, cmdProcessor, documentation, args));
	}
	
	private void executeGetAuthRequest (JsonObject cmd, Handler<JsonObject> handler)
	{
		logDebug ("Executing get authentication info request: " + cmd);
	}
	
	private File createTempFile (byte[] data) throws IOException
	{
		Path tmp = Files.createTempFile ("docker", ".tar.gz");
		Files.write (tmp, data);
		File tmpFile = tmp.toFile ();
		tmpFile.deleteOnExit ();
		return tmpFile;
	}
	
	private void executeGetAuthRequest (JsonObject cmd, RoutingContext routingContext)
	{
		logDebug ("Executing get authentication info request: " + cmd);
		String artifactory = cmd.getString ("artifactory");
		if (Strings.isEmpty (artifactory)) {
			HttpServerResponse response = routingContext.response ();
			response.setStatusCode (404);
			response.end ();
		}
		else {
			String user = cmd.getString ("user");
			String resFileName = (DockerAuthentication.getZipConfigFile () ? artifactory + ".tar.gz" : "config.json");
			String contentType = (DockerAuthentication.getZipConfigFile () ? "application/octet-stream" : "application/json");
			logDebug ("Retrieving authentication info for " + Authentication.buildKey (artifactory, user));
			authStorage.getAuthentication (artifactory, user, res -> {
				HttpServerResponse response = routingContext.response ();
				if (res.succeeded ()) {
					logInfo ("Information obtained. Sending back file.");
					byte[] authData = res.value ().getAuthentication ();
					Buffer buf = Buffer.buffer (authData);
					response
						.setStatusCode (200)
						.putHeader (HttpHeaders.CONTENT_TYPE, contentType)
						.putHeader("Content-Disposition", "attachment; filename=\"" + resFileName + "\"")
						.putHeader (HttpHeaders.CONTENT_LENGTH, Integer.toString (authData.length))
						.write (buf);
					/*
					try {
						File tmpFile = createTempFile (res.value ().getAuthentication ());
						response
							.putHeader (HttpHeaders.CONTENT_TYPE, "application/octet-stream")
							.putHeader("Content-Disposition", "attachment; filename=\\"")
							.setStatusCode (200)
							.sendFile (tmpFile.getAbsolutePath ());
					}
					catch (IOException e) {
						logError ("Failed to send back auth information", e);
						response.setStatusCode (401);
					}
					 */
				}
				else {
					logError ("Failed to retieve auth information.");
					response.setStatusCode (404);
				}
				if (! response.ended ()) {
					response.end ();
				}
			});
		}
	}
	
	private void executeAddAuthRequest (JsonObject cmd, Handler<JsonObject> handler)
	{
		logDebug ("Executing add authentication info request: " + cmd);
		String artifactory = cmd.getString ("artifactory");
		String user = cmd.getString ("user");
		String password = cmd.getString ("password");
		if (Strings.isEmpty (artifactory) || Strings.isEmpty (user) || Strings.isEmpty (password)) {
			handler.handle (JsonTools.buildFail ("Not all required arguments provided"));
		}
		else {
			authStorage.addAuthentication (artifactory, user, password, res -> {
				if (res.succeeded ()) {
					handler.handle (JsonTools.buildSuccess ());
				}
				else {
					handler.handle (JsonTools.buildFail (res.cause ()));
				}
			});
		}
	}
	
	private void executeDeleteAuthRequest (JsonObject cmd, Handler<JsonObject> handler)
	{
		logDebug ("Executing delete authentication info request: " + cmd);
		String artifactory = cmd.getString ("artifactory");
		if (Strings.isEmpty (artifactory)) {
			handler.handle (JsonTools.buildFail ("Not all required arguments provided"));
		}
		else {
			String user = cmd.getString ("user");
			authStorage.deleteAuthentication (artifactory, user, res -> {
				if (res.succeeded ()) {
					handler.handle (JsonTools.buildSuccess ());
				}
				else {
					handler.handle (JsonTools.buildFail (res.cause ()));
				}
			});
		}
	}
	
	private void executeHelpRequest (JsonObject cmd, Handler<JsonObject> handler)
	{
		JsonObject response = buildSuccess (); 
		JsonArray cmds = new JsonArray ();
		for (RequestAction action : commands.values ()) {
			cmds.add (action.toJson ());
		}
		response.put ("commands", cmds);
		handler.handle (response);
	}
	// ----------------------------------------------------------------------------------
	@Override
	protected HttpServerOptions buildHttpServerOptions ()
	{
		return super.buildHttpServerOptions ().setSsl (false);
	}
	
	@Override
	protected void initOnStart ()
	{
		if (!Strings.isEmpty (MongoURI) && !Strings.isEmpty (DBName) && !Strings.isEmpty (DBCollection)) {
			logInfo (String.format ("Adding MongoDB storage %s/%s@%s", DBName, DBCollection, MongoURI));
			authStorage.setBackupStorage (new MongoAuthenticationStorage (vertx, MongoURI, DBName, DBCollection));
		}
	}

	@Override
	protected JsonObject buildCommand (RoutingContext routingContext)
	{
		JsonObject json = super.buildCommand (routingContext);
		logInfo ("buildCommand: json = " + json.encode ());
		HttpServerRequest req = routingContext.request ();
		MultiMap params = req.params ();
		if (params != null && !params.isEmpty ()) {
			logInfo ("Reading parameters: {");
			for (Entry<String, String> param : params.entries ()) {
				logInfo ("  " + param.getKey () + " = " + param.getValue ());
				json.put (param.getKey (), param.getValue ());
			}
			logInfo ("}");
		}
		else {
			logInfo ("No parameters provided");
		}
		return json;
	}
	
	@Override
	protected void appendWebService (Router router)
	{
		router.route ("/get/*").handler (this::processGetRequest);
	}

	@Override
	protected void processHttpRequest (RoutingContext routingContext)
	{
		JsonObject command = buildCommand (routingContext);
		if (command != null && "get".equals (command.getString ("command"))) {
			executeGetAuthRequest (command, routingContext);
		}
		else {
			super.processHttpRequest (routingContext);
		}
	}
	
	private static final String TarGzExt = ".tar.gz";
	private static int TarGzExtLen = TarGzExt.length (); 
	
	private void processGetRequest (RoutingContext routingContext)
	{
		String path = routingContext.normalisedPath ();
		if (! Strings.isEmpty (path)) {
			path = path.substring (5);
			if (path.endsWith (TarGzExt)) {
				String artifactory = path.substring (0, path.length () - TarGzExtLen);
				JsonObject command = new JsonObject ();
				command.put ("command", "get");
				command.put ("artifactory", artifactory);
				executeGetAuthRequest (command, routingContext);
			}
		}
		else {
			super.processHttpRequest (routingContext);
		}
	}

	@Override
	protected void executeRequest (JsonObject req, Handler<JsonObject> handler)
	{
		logger.info ("Request received: " + req);
		String command = req.getString ("command");
		RequestAction processor = (command != null ? commands.get (command) : null);
		if (processor != null) {
			processor.processRequest (req, handler);
		}
		else {
			executeUnknownRequest (req, handler);
		}
	}
	
	private void executeUnknownRequest (JsonObject cmd, Handler<JsonObject> handler)
	{
		handler.handle (JsonTools.buildFail ("Unknown command " + cmd.encode ()));
	}
	
	@Override
	protected Logger getLogger ()
	{
		return logger;
	}

	// -----------------------------------------------------------------
	private static void usage ()
	{
		List<CommandLineArgument> args = ApplicationProperties.collectCommandLineArguments (
				DockerAuthProviderService.class, DockerAuthentication.class);
		if (args != null) {
			System.out.println ("Command line arguments: ");
			System.out.println ("  -config (String) = \"" + DefaultPropertiesFile + "\" - properties config file");
			for (CommandLineArgument arg : args) {
				System.out.println ("  " + arg.toString ());
			}
		}
		System.exit (0);
	}
	
	public static void main (String[] args)
	{
		String propFile = DefaultPropertiesFile;

		CommandLineArguments arguments = parseArguments (args);
		if (arguments.hasArgument ("help") || arguments.hasArgument ("h") || arguments.hasArgument ("?")) {
			usage ();
		}
		if (arguments.hasArgument ("config")) {
			propFile = arguments.getArgument ("config");
		}
		
		ApplicationProperties.loadProperties (propFile, arguments, DockerAuthProviderService.class);
		
		VertxOptions vops = buildVertxOptions ();
		
		runClustered (new DockerAuthProviderService ()
				.setServerPath (WebServerPath)
				.setIndexPage (DebugIndexPage)
				.setDebudPath (DebugPath)
				.setWebRoot (WebRoot),
			vops);
	}
	
}
